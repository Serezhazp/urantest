package com.example.serezha.urantest;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by serezha on 17.11.15.
 */
public class UnixTimestampToDateConverter {

    public UnixTimestampToDateConverter() {
    }

    public String unixTimestampToDateFormat(String unixTime) {
        if(isInteger(unixTime)) {
            long time = Integer.parseInt(unixTime) * (long) 1000;
            Date date = new Date(time);
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault());
            Log.d("date", format.format(date));

            return format.format(date);
        } else {
            return "Can't parse date";
        }
    }

    public static boolean isInteger(String str)
    {
        try {
            long d = Integer.parseInt(str);
        }
        catch(NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
