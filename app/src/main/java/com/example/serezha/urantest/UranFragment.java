package com.example.serezha.urantest;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class UranFragment extends Fragment {
    private String STRING_URL = "http://android-logs.uran.in.ua/test.php";
    private Button buttonBgColor;
    private Button buttonLoadData;
    private RelativeLayout layout;
    private TextView textView;
    private ListView listView;
    private DateArrayAdapter arrayAdapter;
    private int color;
    private String lastDate = "( полученные данные )";

    public UranFragment() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("Color", color);
        outState.putString("LastDate", lastDate);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null) {
            color = savedInstanceState.getInt("Color");
            lastDate = savedInstanceState.getString("LastDate");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if(savedInstanceState == null) {
            Bundle extras = getActivity().getIntent().getExtras();
            if(extras != null) {
                color = extras.getInt("Color");
                lastDate = extras.getString("LastDate");
            }
        } else {
            color = savedInstanceState.getInt("Color");
            lastDate = savedInstanceState.getString("LastDate");
        }

        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ArrayList<String> datesList = MainActivity.db.getAllDates();

        View view = inflater.inflate(R.layout.fragment_uran, container, false);
        buttonBgColor = (Button) view.findViewById(R.id.buttonBgColor);
        buttonLoadData = (Button) view.findViewById(R.id.buttonLoadData);
        textView = (TextView) view.findViewById(R.id.textView);
        textView.setText(lastDate);
        layout = (RelativeLayout) view.findViewById(R.id.layout);
        layout.setBackgroundColor(color);
        listView = (ListView) view.findViewById(R.id.listView);
        arrayAdapter =
                new DateArrayAdapter(getActivity(), R.layout.list_item, datesList);
        listView.setAdapter(arrayAdapter);

        buttonBgColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rnd = new Random();
                color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                layout.setBackgroundColor(color);
            }
        });

        buttonLoadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HTTPClass httpClass = new HTTPClass();
                if (httpClass.networkAvailable(getActivity())) {
                    new GetTestData().execute(STRING_URL);
                } else {
                    textView.setText("No network connection available.");
                }
            }
        });

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public class GetTestData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            HTTPClass httpClass = new HTTPClass();
            try {
                return httpClass.downloadUrl(urls[0]);
            } catch (Exception e) {
                return "Unable to retrieve string";
            }
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            UnixTimestampToDateConverter converter = new UnixTimestampToDateConverter();
            arrayAdapter.clear();
            ArrayList<String> temp = MainActivity.db.getAllDates();
            arrayAdapter = new DateArrayAdapter(getActivity(), R.layout.list_item, temp);
            listView.setAdapter(arrayAdapter);
            if(aVoid != null && !aVoid.isEmpty()) {
                TimeParser parser = new TimeParser(aVoid);
                MainActivity.db.addDate(parser.getTime());
                lastDate = converter.unixTimestampToDateFormat(parser.getTime());
                textView.setText(lastDate);
            }
        }
    }
}
