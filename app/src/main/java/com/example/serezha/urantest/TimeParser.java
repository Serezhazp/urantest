package com.example.serezha.urantest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by serezha on 16.11.15.
 */
public class TimeParser {
    private Pattern pattern;
    private Matcher matcher;
    private String[] intData;
    private String match;

    private static final String UNIX_TIME_PATTERN = "^(?m)[0-9]{10,11}$";

    public TimeParser(String extData){
        pattern = Pattern.compile(UNIX_TIME_PATTERN);
        String temp = extData.replaceAll("\\)", "");
        intData = temp.split(" |\\n");
    }

    public String getTime() {
        for(int i = 0; i < intData.length; i++) {
            matcher = pattern.matcher(intData[i]);
            if(matcher.find()) {
                match = matcher.group();
                System.out.println("I found UNIX time: " + match + " in " + i + "'th line");
                break;
            } else {
                System.out.println(intData[i] + " is not matches");
            }
        }

        return match;
    }
}
