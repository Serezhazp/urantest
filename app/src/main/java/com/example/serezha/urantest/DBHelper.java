package com.example.serezha.urantest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by serezha on 17.11.15.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "UrantestDB";

    private static final String TABLE_URANTEST = "urantest";
    private static final String KEY_ID = "id";
    private static final String KEY_DATE = "date";

    private static final String[] COLUMNS = {
            KEY_ID,
            KEY_DATE,
    };

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_URANTEST + " ( " +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KEY_DATE + " TEXT )";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + TABLE_URANTEST);
        this.onCreate(db);
    }

    public void addDate(String date) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DATE, date);

        db.insert(TABLE_URANTEST, null, values);
        db.close();
    }

    public ArrayList<String> getAllDates() {
        ArrayList<String> datesList = new ArrayList<>();

        String query = "SELECT * FROM " + TABLE_URANTEST;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String date;
        if (cursor.moveToLast()) {
            do {
                date = cursor.getString(1);
                datesList.add(date);
            } while (cursor.moveToPrevious());
        }
        db.close();
        cursor.close();
        Log.d("ALL DATES: \n", datesList.toString());
        return datesList;
    }

    public void clearDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_URANTEST);
        db.close();
    }
}
