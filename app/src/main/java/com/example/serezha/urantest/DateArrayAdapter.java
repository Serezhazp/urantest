package com.example.serezha.urantest;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by serezha on 17.11.15.
 */
public class DateArrayAdapter extends ArrayAdapter<String> {
    private Activity mActivity;
    private ArrayList<String> dates;

    public DateArrayAdapter(Context context, int resource, ArrayList<String> list) {
        super(context, resource, list);

        mActivity = (Activity) context;
        dates = list;
    }

    static class ViewHolder {
        TextView dateTextView;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        UnixTimestampToDateConverter converter = new UnixTimestampToDateConverter();

        if(convertView == null) {
            LayoutInflater inflater = mActivity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.dateTextView = (TextView) convertView.findViewById(R.id.dateTextView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.dateTextView.setText(converter.unixTimestampToDateFormat(dates.get(position)));

        return convertView;
    }


}
